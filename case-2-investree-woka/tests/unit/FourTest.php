<?php

use PHPUnit\Framework\TestCase;
// use Feature\findPrimeByRange;

class FourTests extends TestCase
{
    public function test_primeByRange(){

        $a=11;
        $b=40;

        $a_new=1;
        $b_new=10;

        require 'findPrimeByRange.php';
        //true
        $this->assertTrue([11,13,17,19,23,29,31,37]==findPrimeByRange($a,$b));
        //false
        $this->assertFalse([2,3,5]==findPrimeByRange($a_new,$b_new));
    }

    public function test_palindrome(){

    
        $a="abcba";
        $b="absa";

        require 'palindrome.php';
        //true
        $this->assertTrue("true"==isPalindrome($a));
        //false
        $this->assertFalse("true"==isPalindrome($b));
    }
    public function test_group(){

    
        $a=['a','a','a','b','c','c','b','b','b','d','d','e','e','e'];
        

        require 'groupFunction.php';
        //true
        $this->assertTrue([['a', 'a', 'a'], ['b'], ['c','c'], ['b','b','b'], ['d','d'], ['e','e','e']]==group($a));
        //false
        $this->assertFalse([['a', 'a', 'a'], ['c','c'], ['b','b','b','b'], ['d','d'], ['e','e','e']]==group($a));
    }

    public function test_count(){

    
        $a=['a','a','a','b','c','c','b','b','b','d','d','e','e','e'];
        

        require 'countFunction.php';
        //true
        $this->assertTrue([[3=>'a'],[1=>'b'],[2=>'c'],[3=>'b'],[2=>'d'],[3=>'e']]==countArr($a));
        //false
        $this->assertFalse([[3=>'a'],[4=>'b'],[2=>'c'],[2=>'d'],[3=>'e']]==countArr($a));
    }

}


?>